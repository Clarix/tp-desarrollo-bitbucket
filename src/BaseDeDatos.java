import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class BaseDeDatos {

	public static void agregarCliente(Cliente cliente)
			throws SQLException, ClassNotFoundException {
		try {
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/tp-bitbucket";
			Class.forName(myDriver);
			
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = " insert into cliente (nombre, apellido, mail, monto, dni, nroCuenta)" + " values (?, ?, ?, ?, ?, ?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, cliente.getNombre());
			preparedStmt.setString(2, cliente.getApellido());
			preparedStmt.setString(3, cliente.getMail());
			preparedStmt.setDouble(4, cliente.getMonto());
			preparedStmt.setInt(5, cliente.getDni());
			preparedStmt.setInt(6, cliente.getNroCuenta());

			preparedStmt.executeUpdate();
			conn.close();
			System.out.println("Cliente Guardado");
		} catch (SQLException se) {
			throw se;
		}
	}

}
