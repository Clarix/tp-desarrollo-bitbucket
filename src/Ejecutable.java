import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Ejecutable {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {

		BaseDeDatos bd = new BaseDeDatos();
		Scanner in = new Scanner(System.in);
		
		List<Cliente> listCli = new ArrayList<Cliente>();
		
		for (int i = 1; i < 6; i++) {			
			System.out.println("Ingrese Nombre de Cliente " + i + " :");
			String nombre = in.nextLine();
			
			System.out.println("Ingrese Apellido de Cliente " + i + " :");
			String apellido = in.nextLine();
			
			System.out.println("Ingrese Mail de Cliente " + i + " :");
			String mail = in.nextLine();
			
			System.out.println("Ingrese Monto de Cliente " + i + " :");
			double monto = in.nextDouble();
			in.nextLine();
			
			System.out.println("Ingrese DNI de Cliente (sin puntos) " + i + " :");
			int dni = in.nextInt();
			in.nextLine();
			
			System.out.println("Ingrese N� Cuenta de Cliente " + i + " :");
			int nroCuenta = in.nextInt();
			in.nextLine();
			
			System.out.println("***** -------- ****** ------- ******");
			
			
			Cliente c1 = new Cliente(nombre, apellido, mail, monto, dni, nroCuenta);
			listCli.add(c1);
			bd.agregarCliente(c1);
		}
		
		System.out.println("/////// NOMBRES CLIENTES //////");
		
		listCli.stream().forEach((c)-> {
			System.out.println(c.getNombre());
		});
		
		System.out.println("/////// CONSULTAR MONTO POR DNI (MEMORIA) //////");
		System.out.println("Ingrese DNI :");
		int dniConsulta = in.nextInt();
		in.nextLine();
		listCli.get(0).getMontoByDni(dniConsulta, listCli);
		
		System.out.println("/////// CONSULTAR MONTO POR DNI (DB) //////");
		System.out.println("Ingrese DNI :");
		dniConsulta = in.nextInt();
		in.nextLine();
		listCli.get(0).getMontoByDni(dniConsulta);
		
	}
}
