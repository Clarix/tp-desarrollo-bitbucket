import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;


public class Cliente {
	private String nombre;
	private String apellido;
	private String mail;
	private double monto;
	private int dni;
	private int nroCuenta;

	public Cliente(String nombre, String apellido, String mail, double monto, int dni, int nroCuenta) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.monto = monto;
		this.dni = dni;
		this.nroCuenta = nroCuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(int nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public void getMontoByDni(int dni, List<Cliente> lista) {
		Optional<Cliente> res = lista.stream().filter(p -> p.getDni() == dni).
	    findFirst();
		
		if (res.isPresent()) {
			System.out.println("Monto : " + res.get().getMonto());
		} else {
			System.out.println("No se encontr� Cliente con el DNI ingresado");
		}
	}
	
	public static void getMontoByDni(int dni)
			throws SQLException, ClassNotFoundException {
		try {
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/tp-bitbucket";
			Class.forName(myDriver);
			
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = "select monto from cliente where dni = " + dni;
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
		      while (rs.next()) {		    	  
		        float monto = rs.getFloat("monto");
		        System.out.println("Monto : " + monto);
		      }
			conn.close();
		} catch (SQLException se) {
			throw se;
		}
	}
	
	public void saludar() {
		System.out.println("hola soy" + this.getNombre());
	}

}